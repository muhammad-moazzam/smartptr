#include <iostream>
#include "database.h"
using namespace std;

emumba::training::Database::Database()
{
    stdObjVector.reserve(100);
    Student std1 = Student("ahmed", 10, 3.2);
    Student std2 = Student("ali", 20, 1.2);
    Student std3 = Student("akbar", 30, 3.2);
    Student std4 = Student("azam", 40, 2.2);
    Student std5 = Student("ahmaq", 50, 2.2);
    stdObjVector.push_back(std1);
    stdObjVector.push_back(std2);
    stdObjVector.push_back(std3);
    stdObjVector.push_back(std4);
    stdObjVector.push_back(std5);
    spMap[std1.getName()] = make_shared<Student>(std1); //std::shared_ptr<Student>(&stdObjVector.back());
    spMap[std2.getName()] = make_shared<Student>(std2);
    spMap[std3.getName()] = make_shared<Student>(std3);
    spMap[std4.getName()] = make_shared<Student>(std4);
    spMap[std5.getName()] = make_shared<Student>(std5);
}

const shared_ptr<emumba::training::Student> emumba::training::Database::get_student_reference(string name) const
{
    auto itr = spMap.find(name);
    if (itr != spMap.end())
    {
        return itr->second;
    }
    else
    {
        return nullptr;
    }
}

unique_ptr<emumba::training::Student> emumba::training::Database::get_unique_student_reference(std::string name) const
{
    auto ptr = stdObjVector.begin();
    for (; ptr < stdObjVector.end(); ptr++)
    {
        auto check_name = *ptr;
        if (check_name.getName() == name)
        {
            return make_unique<Student>(check_name);
        }
    }
    return nullptr;
}