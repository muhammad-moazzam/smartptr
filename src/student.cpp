#include <iostream>
#include "student.h"
#include <string>

using namespace std;

emumba::training::Student::Student(string name, int age, float cgpa)
{
    self.name = name;
    self.age = age;
    self.cgpa = cgpa;
}

void emumba::training::Student::set_subject_marks(const string &subject, const int &marks)
{
    resultMap[subject] = marks;
}

const int emumba::training::Student::get_subject_marks(const string &subject) const
{
    auto itr = resultMap.find(subject);
    if (itr != resultMap.end())
    {
        return itr->second;
    }
    else
    {
        return -1;
    }
}

void emumba::training::Student::print_all_marks() const
{
    if (resultMap.begin() != resultMap.end())
    {
        for (auto itr = resultMap.begin(); itr != resultMap.end(); itr++)
        {
            cout << "Subject: " << (*itr).first << " Marks: " << (*itr).second << "\n";
        }
    }
    else
    {
        cout << "No subject is registered";
    }
}

string emumba::training::Student::getName() const
{
    return self.name;
}

const float emumba::training::Student::get_cgpa() const
{
    return self.cgpa;
}

const int emumba::training::Student::get_age() const
{
    return self.age;
}

emumba::training::Student::~Student()
{
    cout << "Destructor is called" << endl;
}
