#include <iostream>
#include "student.h"
#include "database.h"
using namespace std;

void print_student_detail(const unique_ptr<emumba::training::Student> &up_)
{
    if (up_ != nullptr)
    {
        cout << up_->getName() << endl;
        cout << up_->get_age() << endl;
        cout << up_->get_cgpa() << endl;
    }
    else
    {
        cout << "No such Student exists" << endl;
    }
}

int main()
{
    emumba::training::Database db;
    shared_ptr<emumba::training::Student> sharedStdptr = db.get_student_reference("ahmed");
    sharedStdptr->set_subject_marks("Physics", 100);
    cout << endl
         << sharedStdptr->get_subject_marks("Physics");
    shared_ptr<emumba::training::Student> sharedStdptr2 = db.get_student_reference("ahmed");
    cout << endl
         << sharedStdptr2->get_subject_marks("Physics");
    auto unique_ptr = db.get_unique_student_reference("akbar");
    sharedStdptr2->set_subject_marks("Physics", 30);
    cout << sharedStdptr->get_subject_marks("Physics") << endl;
    print_student_detail(unique_ptr);
    return 0;
}
