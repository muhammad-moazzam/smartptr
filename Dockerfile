ARG VERSION=20.04

FROM ubuntu:${VERSION}

COPY . /usr/src/cmake/

WORKDIR /usr/src/cmake/

RUN apt-get update -y

RUN apt-get -y install cmake make python3 g++

RUN apt-get -y install libboost-all-dev

RUN apt-get -y install git

RUN cd build/ && cmake .. && make

CMD ["/usr/src/cmake/build/app/Debug/CmakeTask"]

