#ifndef DATABASE_H
#define DATABASE_H
#include "student.h"
#include "vector"
#include "map"
#include <memory>
namespace emumba
{
    namespace training
    {
        class Database
        {
        private:
            std::vector<Student> stdObjVector;
            std::map<std::string, std::shared_ptr<Student>> spMap;

        public:
            Database();
            const std::shared_ptr<emumba::training::Student> get_student_reference(std::string name) const;
            std::unique_ptr<emumba::training::Student> get_unique_student_reference(std::string name) const;
        };
    }
}
#endif