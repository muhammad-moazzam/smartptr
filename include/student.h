#ifndef STUDENT_H
#define STUDENT_H
#include <string>
#include <map>
#include <iostream>
namespace emumba
{
    namespace training
    {
        class Student
        {
        private:
            struct student_record
            {
                std::string name;
                unsigned int age;
                float cgpa;
            } self;

            std::map<std::string, int> resultMap;

        public:
            Student(std::string roll_no, int age, float cgpa);
            const int get_subject_marks(const std::string &subject) const;
            void set_subject_marks(const std::string &subject, const int &marks);
            void print_all_marks() const;
            std::string getName() const;
            const int get_age() const;
            const float get_cgpa() const;
            ~Student();
        };
    }
}
#endif